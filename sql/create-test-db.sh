#!/usr/bin/env bash
psql -c "drop database if exists test_db_local_schedule;" -U Maxim;
psql -c "create database test_db_local_schedule;" -U Maxim;
cd sql;
psql -U Maxim -f structure-shed.sql test_db_local_schedule;
