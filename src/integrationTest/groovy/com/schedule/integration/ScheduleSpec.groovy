package com.schedule.integration

import com.jayway.restassured.response.Response
import com.schedule.data.model.Schedule
import com.schedule.data.model.SchoolMember
import com.schedule.data.repo.AttendanceRepository
import com.schedule.data.repo.GlobalMarkRepository
import com.schedule.data.repo.HomeworkRepository
import com.schedule.data.repo.LessonRepository
import com.schedule.data.repo.MarkRepository
import com.schedule.data.repo.ScheduleRepository
import com.schedule.data.repo.SchoolClassRepository
import com.schedule.data.repo.SchoolMemberRepository
import com.schedule.data.repo.StudentRepository
import com.schedule.data.repo.VersionRepository
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional

import static com.jayway.restassured.RestAssured.given

/**
 * Created by Maxim on 6/8/16.
 */
@Transactional
class ScheduleSpec extends RestIntegrationBaseSpec{
    @Autowired
    AttendanceRepository attendanceRepository
    @Autowired
    GlobalMarkRepository globalMarkRepository
    @Autowired
    HomeworkRepository homeworkRepository
    @Autowired
    LessonRepository lessonRepository
    @Autowired
    MarkRepository markRepository
    @Autowired
    ScheduleRepository scheduleRepository
    @Autowired
    SchoolClassRepository schoolClassRepository
    @Autowired
    SchoolMemberRepository schoolMemberRepository
    @Autowired
    VersionRepository versionRepository
    @Autowired
    StudentRepository studentRepository

//    def setupSpec(){
//        executeSql("DELETE FROM attendances")
//        executeSql("DELETE FROM global_marks")
//        executeSql("DELETE FROM homeworks")
//        executeSql("DELETE FROM lessons")
//        executeSql("DELETE FROM marks")
//        executeSql("DELETE FROM schedules")
//        executeSql("DELETE FROM classes")
//        executeSql("DELETE FROM school_members")
//        executeSql("DELETE FROM students")
//        executeSql("DELETE FROM versions")
//    }

    def setup(){
        executeSql("INSERT INTO attendances (id,user_id,schedule_id,is_present,date_attendance) VALUES (1,2,1,TRUE,'2016-05-25 12:30:20.375069')")
        executeSql("INSERT INTO schedules (id,teacher_id,room,class_id,lesson_id,day_of_week,time_of_lesson,version_id) VALUES (1,6,'302',1,2,3,'08:30:00.000000',1)")

        executeSql("INSERT INTO students (id,email,name,phone,class_id) " +
                "VALUES (1,'pupil@email.com','Pupil Ivan','056111111',1)")
        executeSql("INSERT INTO school_members (id,email,role_id,name,phone) " +
                "VALUES (6,'teacher@email.com',3,'Teacher Serg','056222222')")
        executeSql("INSERT INTO school_members (id,email,role_id,name,phone) " +
                "VALUES (7,'Classteacher@email.com',3,'ClssTecher Ivy','056222222')")

        executeSql("INSERT INTO classes (id,name) VALUES (1,'1A')")
        executeSql("INSERT INTO lessons (id,name) VALUES (2,'English')")
        executeSql("INSERT INTO versions (id,date_of_version,schedule_id) VALUES (1,'2016-05-25 12:30:20.375069',1)")
    }
//    def cleanup(){
//        executeSql("DELETE FROM attendances")
//        executeSql("DELETE FROM global_marks")
//        executeSql("DELETE FROM homeworks")
//        executeSql("DELETE FROM lessons")
//        executeSql("DELETE FROM marks")
//        executeSql("DELETE FROM schedules")
//        executeSql("DELETE FROM classes")
//        executeSql("DELETE FROM school_members")
//        executeSql("DELETE FROM students")
//        executeSql("DELETE FROM versions")
//    }

//    def "retrieve schedule by id"(){
//        when:
//        Schedule schedule = scheduleRepository.findOne(1)
//        def a = 5
//        then:
//        assert a == 5
//        assert schedule.teacher.id == 6
//        assert schedule.room == "302"
//        assert schedule.schoolClass.id == 1
//        assert schedule.lesson.id == 2
//        assert schedule.dayOfWeek == 3
//        assert schedule.timeOfLesson.toString() == '08:30'
////        assert schedule.versions.first().id == 1
////                executeSql("INSERT INTO schedules (id,teacher_id,room,class_id,lesson_id,day_of_week,time_of_lesson,version_id) VALUES (1,6,'302',1,2,3,'2016-06-05 08:30:00.000000',1)")
//    }

    def "retrieve schedule by user_id"(){
        when:
        String url = serviceURI("school-schedule")
        Response resp = given().auth().basic("sample","12344321").queryParam("userid", "1").log().all().get(url + "/user")
        then:
//        assert resp.statusCode == 200
        println resp.body.asString()
        assert 1 ==1
//        def JSON = new JsonSlurper().parseText(resp.body.asString())
//        assert JSON.ad_format_id == 123
//        assert JSON.app_id == 22
//        assert JSON.rank == 179303.278688525
//        assert JSON.updated_at == '2015-08-16 07:00:01.0'
    }
}
