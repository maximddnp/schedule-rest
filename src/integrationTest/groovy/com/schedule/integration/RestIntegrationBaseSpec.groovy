package com.schedule.integration

import com.schedule.data.Application
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.boot.test.WebIntegrationTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

/**
 * Created by Maxim on 6/8/16.
 */
@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = [Application.class] )
@ActiveProfiles("test")
@WebIntegrationTest
@Stepwise
class RestIntegrationBaseSpec extends Specification {




    @Value('${local.server.port}')
    int port

    String getBasePath() { "rest/" }

    URI serviceURI(String path = "") {
        new URI("http://localhost:$port/${basePath}${path}")
    }

    def db = [url: "jdbc:postgresql://localhost:5432/test_db_local_schedule", user: 'Maxim', password: '', driver: 'org.postgresql.Driver']

    def executeSql(String sqlStr){
        def sql = Sql.newInstance(db.url, db.user, db.password, db.driver)
        sql.execute(sqlStr)
        sql.close()
        println "close connection"
    }

}

