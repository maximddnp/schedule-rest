--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: schedules; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schedules (
    id integer NOT NULL,
    teacher_id integer,
    room character varying(255),
    class_id integer NOT NULL,
    lesson_id integer NOT NULL,
    day_of_week integer NOT NULL,
    time_of_lesson time without time zone,
    version_id integer NOT NULL
);

--
-- Name: schedules_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE schedules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: schedules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE schedules_id_seq OWNED BY schedules.id;

--
-- Name: school_members; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE school_members (
    id integer NOT NULL,
    email character varying(255) NOT NULL,
    role_id integer,
    name character varying(255),
    password_digest character varying(255),
    confirmation_token character varying(255),
    reset_password_token character varying(255),
    phone character varying(255)
);

--
-- Name: school_members_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE school_members_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: school_members_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE school_members_id_seq OWNED BY school_members.id;

--
-- Name: students; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE students (
    id integer NOT NULL,
    email character varying(255) NOT NULL,
    name character varying(255),
    password_digest character varying(255),
    confirmation_token character varying(255),
    reset_password_token character varying(255),
    phone character varying(255),
    class_id integer
);

--
-- Name: students_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE students_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: students_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE students_id_seq OWNED BY students.id;


--
-- Name: classes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE classes (
    id integer NOT NULL,
    name character varying(255)
);

--
-- Name: classes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE classes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: classes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE classes_id_seq OWNED BY classes.id;

--
-- Name: lessons; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE lessons (
    id integer NOT NULL,
    name character varying(255)
);

--
-- Name: lessons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE lessons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lessons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE lessons_id_seq OWNED BY lessons.id;

--
-- Name: attendances; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE attendances (
    id integer NOT NULL,
    user_id integer,
    schedule_id integer,
    is_present boolean DEFAULT false NOT NULL,
    date_attendance timestamp without time zone NOT NULL
);


--
-- Name: attendances_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE attendances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: attendances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE attendances_id_seq OWNED BY attendances.id;

--
-- Name: marks; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE marks (
    id integer NOT NULL,
    user_id integer,
    schedule_id integer,
    mark integer,
    date_of_mark timestamp without time zone NOT NULL,
    comments character varying(255)
);


--
-- Name: marks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE marks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: marks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE marks_id_seq OWNED BY marks.id;

--
-- Name: global_marks; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE global_marks (
    id integer NOT NULL,
    user_id integer,
    mark integer,
    date_of_mark timestamp without time zone NOT NULL,
    type integer,
    comments character varying(255)
);

--
-- Name: global_marks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE global_marks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: global_marks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE global_marks_id_seq OWNED BY global_marks.id;

--
-- Name: homeworks; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE homeworks (
    id integer NOT NULL,
    schedule_id integer,
    student_id integer,
    class_id integer,
    description character varying(255),
    date_of_homework timestamp without time zone NOT NULL
);

--
-- Name: homeworks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE homeworks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: homeworks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE homeworks_id_seq OWNED BY global_marks.id;

--
-- Name: versions; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE versions (
    id integer NOT NULL,
    date_of_version timestamp without time zone NOT NULL,
    schedule_id integer NOT NULL
);

--
-- Name: versions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE versions_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- Name: versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE versions_id_seq OWNED BY versions.id;
