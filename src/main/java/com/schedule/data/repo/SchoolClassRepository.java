package com.schedule.data.repo;

import com.schedule.data.model.SchoolClass;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by Maxim on 6/7/16.
 */
@RepositoryRestResource(collectionResourceRel = "class", path = "class")
public interface SchoolClassRepository extends PagingAndSortingRepository<SchoolClass, Integer>{
}
