package com.schedule.data.repo;

import com.schedule.data.model.Student;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Maxim on 6/9/16.
 */
@RepositoryRestResource(collectionResourceRel = "student", path = "student")
public interface StudentRepository extends PagingAndSortingRepository<Student, Integer> {
    List<Student> findByName(@Param("name") String name);
    List<Student> findByEmail(@Param("email") String email);
}
