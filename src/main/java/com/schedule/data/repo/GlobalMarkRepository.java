package com.schedule.data.repo;

import com.schedule.data.model.GlobalMark;
import com.schedule.data.model.Mark;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Maxim on 6/7/16.
 */
@RepositoryRestResource(collectionResourceRel = "global_mark", path = "global_mark")
public interface GlobalMarkRepository extends PagingAndSortingRepository<GlobalMark, Integer> {
    List<GlobalMark> findByUserId(@Param("user_id") Integer userId);
}
