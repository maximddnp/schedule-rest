package com.schedule.data.repo;

import com.schedule.data.model.Homework;
import com.schedule.data.model.Schedule;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Maxim on 6/7/16.
 */
@RepositoryRestResource(collectionResourceRel = "homework", path = "homework")
public interface HomeworkRepository extends PagingAndSortingRepository<Homework, Integer> {
    List<Homework> findByClassId(@Param("class_id") Integer classId);
}
