package com.schedule.data.repo;

import com.schedule.data.model.SchoolMember;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Maxim on 6/7/16.
 */
@RepositoryRestResource(collectionResourceRel = "school_member", path = "school_member")
public interface SchoolMemberRepository extends PagingAndSortingRepository<SchoolMember, Integer> {
    List<SchoolMember> findByName(@Param("name") String name);
    List<SchoolMember> findByEmail(@Param("email") String email);
}
