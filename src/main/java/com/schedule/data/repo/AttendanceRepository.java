package com.schedule.data.repo;

import com.schedule.data.model.Attendance;
import com.schedule.data.model.GlobalMark;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Maxim on 6/7/16.
 */
@RepositoryRestResource(collectionResourceRel = "attendance", path = "attendance")
public interface AttendanceRepository extends PagingAndSortingRepository<Attendance, Integer> {
    List<Attendance> findByUserId(@Param("user_id") Integer userId);
}
