package com.schedule.data.repo;

import com.schedule.data.model.Version;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by Maxim on 6/7/16.
 */
@RepositoryRestResource(collectionResourceRel = "version", path = "version")
public interface VersionRepository extends PagingAndSortingRepository<Version, Integer> {
}
