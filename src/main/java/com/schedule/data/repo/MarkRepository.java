package com.schedule.data.repo;

import com.schedule.data.model.Homework;
import com.schedule.data.model.Mark;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Maxim on 6/7/16.
 */
@RepositoryRestResource(collectionResourceRel = "mark", path = "mark")
public interface MarkRepository extends PagingAndSortingRepository<Mark, Integer> {
    List<Mark> findByUserId(@Param("user_id") Integer userId);
}
