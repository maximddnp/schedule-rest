package com.schedule.data.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.schedule.data.Application;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

/**
 * Created by Maxim on 6/6/16.
 */
@Entity(name = "schedule")
@Table(name = "schedules")
public class Schedule {
    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "schedules_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    @Column(name = "id", unique = true, nullable = false, columnDefinition = "serial NOT NULL")
    private int id;

    @Column(name = "teacher_id")
    private Integer teacherId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "teacher_id", insertable = false, updatable = false)
    @RestResource(rel = "school_member", path = "school_member")
    private SchoolMember teacher;

    @Column(name = "room")
    private String room;

    @Column(name = "class_id")
    private Integer classId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "class_id", insertable = false, updatable = false)
    @RestResource(rel = "class", path = "class")
    private SchoolClass schoolClass;

    @Column(name = "lesson_id")
    private Integer lessonId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lesson_id", insertable = false, updatable = false)
    @RestResource(rel = "lesson", path = "lesson")
    private Lesson lesson;

    @Column(name = "day_of_week")
    private Integer dayOfWeek;

    @JsonDeserialize(using = Application.LocalTimeJsonDeserializer.class)
    @JsonSerialize(using = Application.LocalTimeJsonSerializer.class)
//    @JsonFormat(pattern="HH:mm:ss")
//    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Column(name = "time_of_lesson", columnDefinition = "timestamp without time zone")
    private LocalTime timeOfLesson;

    public Integer getVersionId() {
        return versionId;
    }

    public void setVersionId(Integer versionId) {
        this.versionId = versionId;
    }

    @Column(name = "version_id")
    private Integer versionId;

    @OneToMany(mappedBy = "schedule")
    @RestResource(path = "versions", rel = "versions")
    private List<Version> versions;

    @JsonIgnore
    @OneToMany(mappedBy = "schedule")
    @RestResource(path = "attendances", rel = "attendances")
    private List<Attendance> attendances;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SchoolMember getTeacher() {
        return teacher;
    }

    public void setTeacher(SchoolMember teacher) {
        this.teacher = teacher;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public SchoolClass getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(SchoolClass schoolClass) {
        this.schoolClass = schoolClass;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public LocalTime getTimeOfLesson() {
        return timeOfLesson;
    }

    public void setTimeOfLesson(LocalTime timeOfLesson) {
        this.timeOfLesson = timeOfLesson;
    }

    public List<Version> getVersions() {
        return versions;
    }

    public void setVersions(List<Version> versions) {
        this.versions = versions;
    }

    public List<Attendance> getAttendances() {
        return attendances;
    }

    public void setAttendances(List<Attendance> attendances) {
        this.attendances = attendances;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getLessonId() {
        return lessonId;
    }

    public void setLessonId(Integer lessonId) {
        this.lessonId = lessonId;
    }
}
