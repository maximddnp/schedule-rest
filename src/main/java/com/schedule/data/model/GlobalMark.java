package com.schedule.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.schedule.data.Application;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Created by Maxim on 6/6/16.
 */
@Entity(name = "global_mark")
@Table(name = "global_marks")
public class GlobalMark {
    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "global_marks_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    @Column(name = "id", unique = true, nullable = false, columnDefinition = "serial NOT NULL")
    private int id;

    @Column(name = "user_id")
    private Integer userId;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    @RestResource(rel = "student", path = "student")
    private Student student;

    @Column(name = "mark")
    private Integer mark;

    @JsonDeserialize(using = Application.LocalDateJsonDeserializer.class)
    @JsonSerialize(using = Application.LocalDateJsonSerializer.class)
//    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Column(name = "date_of_mark", columnDefinition = "timestamp without time zone")
    private LocalDate dateOfMark;

    @Column(name = "type")
    private Integer type;

    @Column(name = "comments")
    private String comments;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public LocalDate getDateOfMark() {
        return dateOfMark;
    }

    public void setDateOfMark(LocalDate dateOfMark) {
        this.dateOfMark = dateOfMark;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
