package com.schedule.data.model;

import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;

/**
 * Created by Maxim on 6/6/16.
 */
@Entity(name = "class")
@Table(name = "classes")
public class SchoolClass {
    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "classes_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    @Column(name = "id", unique = true, nullable = false, columnDefinition = "serial NOT NULL")
    private int id;

    @Column(name = "name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
