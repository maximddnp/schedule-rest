package com.schedule.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Maxim on 6/6/16.
 */
@Entity(name = "version")
@Table(name = "versions")
public class Version {
    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "versions_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    @Column(name = "id", unique = true, nullable = false, columnDefinition = "serial NOT NULL")
    private int id;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Column(name = "date_of_version", columnDefinition = "timestamp without time zone")
    private Date dateOfVersion;

    @Column(name = "schedule_id")
    private Integer scheduleId;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schedule_id", insertable = false, updatable = false)
    @RestResource(rel = "schedule", path = "schedule")
    private Schedule schedule;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateOfVersion() {
        return dateOfVersion;
    }

    public void setDateOfVersion(Date dateOfVersion) {
        this.dateOfVersion = dateOfVersion;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }
}
