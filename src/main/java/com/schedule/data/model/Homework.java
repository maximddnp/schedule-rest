package com.schedule.data.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.schedule.data.Application;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by Maxim on 6/6/16.
 */
@Entity(name = "homework")
@Table(name = "homeworks")
public class Homework {
    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "homeworks_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    @Column(name = "id", unique = true, nullable = false, columnDefinition = "serial NOT NULL")
    private int id;

    @Column(name = "schedule_id")
    private Integer scheduleId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schedule_id", insertable = false, updatable = false)
    @RestResource(rel = "schedule", path = "schedule")
    private Schedule schedule;

    @Column(name = "student_id")
    private Integer studentId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id", insertable = false, updatable = false)
    @RestResource(rel = "student", path = "student")
    private Student student;

    @Column(name = "class_id")
    private Integer classId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "class_id", insertable = false, updatable = false)
    @RestResource(rel = "class", path = "class")
    private SchoolClass schoolClass;

    @Column(name = "description")
    private String description;

    @JsonDeserialize(using = Application.LocalDateTimeJsonDeserializer.class)
    @JsonSerialize(using = Application.LocalDateTimeJsonSerializer.class)
//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSSSSS")
//    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Column(name = "date_of_homework", columnDefinition = "timestamp without time zone")
    private LocalDateTime dateOfHomework;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getDateOfHomework() {
        return dateOfHomework;
    }

    public void setDateOfHomework(LocalDateTime dateOfHomework) {
        this.dateOfHomework = dateOfHomework;
    }

    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public SchoolClass getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(SchoolClass schoolClass) {
        this.schoolClass = schoolClass;
    }
}
