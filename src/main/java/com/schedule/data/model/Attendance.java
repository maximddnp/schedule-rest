package com.schedule.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.schedule.data.Application;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by Maxim on 6/6/16.
 */
@Entity(name = "attendence")
@Table(name = "attendances")
public class Attendance {
    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "attendances_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    @Column(name = "id", unique = true, nullable = false, columnDefinition = "serial NOT NULL")
    private int id;

    @Column(name = "user_id")
    private Integer userId;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    @RestResource(rel = "student_item", path = "student_item")
    private Student student;

    @Column(name = "schedule_id")
    private Integer scheduleId;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schedule_id", insertable = false, updatable = false)
    @RestResource(rel = "schedule_item", path = "schedule_item")
    private Schedule schedule;

    @Column(name = "is_present")
    private Boolean isPresent;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonDeserialize(using = Application.LocalDateTimeJsonDeserializer.class)
    @JsonSerialize(using = Application.LocalDateTimeJsonSerializer.class)
    @Column(name = "date_attendance", columnDefinition = "timestamp without time zone")
    private LocalDateTime dateAttendance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Boolean getPresent() {
        return isPresent;
    }

    public void setPresent(Boolean present) {
        isPresent = present;
    }

    public LocalDateTime getDateAttendance() {
        return dateAttendance;
    }

    public void setDateAttendance(LocalDateTime dateAttendance) {
        this.dateAttendance = dateAttendance;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
