package com.schedule.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Maxim on 6/4/16.
 */
@SpringBootApplication
@Import({RepositoryConfiguration.class, SecurityConfiguration.class})
@EntityScan(basePackageClasses = { Application.class, Jsr310JpaConverters.class })
@EnableCaching
public class Application {
    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC"));
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        Jackson2ObjectMapperBuilder b = new Jackson2ObjectMapperBuilder();
        b.indentOutput(true)
//         .dateFormat(new SimpleDateFormat("yyyy-MM-dd"))
                .serializationInclusion(JsonInclude.Include.NON_EMPTY)
                //.deserializerByType(LocalDateTime.class, new LocalDateTimeJsonDeserializer())
                //.serializerByType(LocalDateTime.class, new LocalDateTimeJsonSerializer())
                .propertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        return b;
    }

    public static final class LocalDateTimeJsonDeserializer extends JsonDeserializer<LocalDateTime> {

        @Override
        public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {
            String text = p.getValueAsString();
            if (text == null || text.isEmpty()) {
                return null;
            }

            return LocalDateTime.parse(text, LocalDateTimeJsonSerializer.FORMATTER);
        }
    }

    public static final class LocalTimeJsonDeserializer extends JsonDeserializer<LocalTime>{

        @Override
        public LocalTime deserialize(JsonParser p, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {
            String text = p.getValueAsString();
            if (text == null || text.isEmpty()) {
                return null;
            }

            return LocalTime.parse(text, LocalTimeJsonSerializer.FORMATTER);
        }
    }

    public static final class LocalDateJsonDeserializer extends JsonDeserializer<LocalDate>{

        @Override
        public LocalDate deserialize(JsonParser p, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {
            String text = p.getValueAsString();
            if (text == null || text.isEmpty()) {
                return null;
            }

            return LocalDate.parse(text, LocalDateJsonSerializer.FORMATTER);
        }
    }


    public static final class LocalDateTimeJsonSerializer extends JsonSerializer<LocalDateTime> {

        public static final DateTimeFormatter FORMATTER = new DateTimeFormatterBuilder()
                .appendPattern("yyyy-MM-dd HH:mm")
                .toFormatter(Locale.UK)
                .withZone(ZoneId.of("UTC"));

        @Override
        public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider serializers)

                throws IOException {
            if (value == null) {
                gen.writeNull();
                return;
            }
            gen.writeString(FORMATTER.format(value));
        }
    }

    public static final class LocalTimeJsonSerializer extends JsonSerializer<LocalTime> {

        public static final DateTimeFormatter FORMATTER = new DateTimeFormatterBuilder()
                .appendPattern("HH:mm:ss")
                .toFormatter(Locale.UK)
                .withZone(ZoneId.of("UTC"));

        @Override
        public void serialize(LocalTime value, JsonGenerator gen, SerializerProvider serializers)

                throws IOException {
            if (value == null) {
                gen.writeNull();
                return;
            }
            gen.writeString(FORMATTER.format(value));
        }
    }

    public static final class LocalDateJsonSerializer extends JsonSerializer<LocalDate> {

        public static final DateTimeFormatter FORMATTER = new DateTimeFormatterBuilder()
                .appendPattern("yyyy-MM-dd")
                .toFormatter(Locale.UK)
                .withZone(ZoneId.of("UTC"));

        @Override
        public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider serializers)

                throws IOException {
            if (value == null) {
                gen.writeNull();
                return;
            }
            gen.writeString(FORMATTER.format(value));
        }
    }
//    @JsonInclude(JsonInclude.Include.NON_EMPTY)
//    //@JsonIgnoreProperties(ignoreUnknown = true)
//    public static class User {
//
//
//        private int id;
//        @JsonDeserialize(using = LocalDateTimeJsonDeserializer.class)
//        @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
//        @JsonProperty("birth_day")
//        private LocalDateTime birthDay;
//
//        public int getId() {
//            return id;
//        }
//
//        public void setId(int id) {
//            this.id = id;
//        }
//
//        public LocalDateTime getBirthDay() {
//            return birthDay;
//        }
//
//        public void setBirthDay(LocalDateTime birthDay) {
//            this.birthDay = birthDay;
//        }
//    }
}
