package com.schedule.data.controller;

import com.schedule.data.model.Schedule;
import com.schedule.data.model.SchoolClass;
import com.schedule.data.model.Student;
import com.schedule.data.repo.ScheduleRepository;
import com.schedule.data.repo.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

import java.util.List;

/**
 * Created by Maxim on 6/4/16.
 */
@RestController
@RequestMapping("/school-schedule")
public class ScheduleController {
    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    StudentRepository studentRepository;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<Schedule> getSchedule(@RequestParam(value = "userid") final String id){
        Student student = studentRepository.findOne(Integer.parseInt(id));
        if (student != null){
            Integer schoolClassId1 = student.getClassId();
            if (schoolClassId1 != null){
                return scheduleRepository.findByClassId(schoolClassId1);
            }
        }
        return null;
    }


}
