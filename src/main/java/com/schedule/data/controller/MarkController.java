package com.schedule.data.controller;

import com.schedule.data.model.Homework;
import com.schedule.data.model.Mark;
import com.schedule.data.model.Student;
import com.schedule.data.repo.HomeworkRepository;
import com.schedule.data.repo.MarkRepository;
import com.schedule.data.repo.ScheduleRepository;
import com.schedule.data.repo.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Maxim on 6/29/16.
 */
@RestController
@RequestMapping("/mark")
public class MarkController {
    @Autowired
    MarkRepository markRepository;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<Mark> getMarks(@RequestParam(value = "userid") final String id){
        return markRepository.findByUserId(Integer.parseInt(id));
    }
}
