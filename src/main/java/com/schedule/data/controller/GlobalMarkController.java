package com.schedule.data.controller;

import com.schedule.data.model.GlobalMark;
import com.schedule.data.model.Mark;
import com.schedule.data.repo.GlobalMarkRepository;
import com.schedule.data.repo.MarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Maxim on 6/29/16.
 */
@RestController
@RequestMapping("/global_mark")
public class GlobalMarkController {
    @Autowired
    GlobalMarkRepository globalMarkRepository;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<GlobalMark> getGlobalMarks(@RequestParam(value = "userid") final String id){
        return globalMarkRepository.findByUserId(Integer.parseInt(id));
    }
}
