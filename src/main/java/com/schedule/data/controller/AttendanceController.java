package com.schedule.data.controller;

import com.schedule.data.model.Attendance;
import com.schedule.data.model.Mark;
import com.schedule.data.repo.AttendanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Maxim on 6/29/16.
 */
@RestController
@RequestMapping("/attendance")
public class AttendanceController {
    @Autowired
    AttendanceRepository attendanceRepository;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<Attendance> getAttendance(@RequestParam(value = "userid") final String id){
        return attendanceRepository.findByUserId(Integer.parseInt(id));
    }
}
