package com.schedule.data.controller;

import com.schedule.data.model.Homework;
import com.schedule.data.model.Schedule;
import com.schedule.data.model.Student;
import com.schedule.data.repo.HomeworkRepository;
import com.schedule.data.repo.ScheduleRepository;
import com.schedule.data.repo.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Maxim on 6/24/16.
 */
@RestController
@RequestMapping("/homework")
public class HomeworkController {
    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    HomeworkRepository homeworkRepository;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<Homework> getHomework(@RequestParam(value = "userid") final String id){
        Student student = studentRepository.findOne(Integer.parseInt(id));
        if (student != null){
            Integer schoolClassId1 = student.getClassId();
            if (schoolClassId1 != null){
                return homeworkRepository.findByClassId(schoolClassId1);
            }
        }
        return null;
    }
}
